<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>WAFEPA - Add/Edit activity</title>
</head>
<body>
	<h1>WAFEPA - Add/Edit activity</h1>
	
	<c:url value="/activities" var="activitiesUrl" />
	
	<form:form action="${activitiesUrl}" method="post" modelAttribute="activity">
		<fieldset>
			<form:hidden path="id"/>
			<form:label path="name">Name</form:label>
			<form:input path="name" />
			<form:errors path="name" cssStyle="color:red"></form:errors>
		</fieldset>
		<p><button type="submit">Submit</button></p>
	</form:form>
</body>
</html>