<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<title>WAFEPA - Users</title>
	</head>
	<body>
		<h1>Users</h1>
		
		<table>
			<thead>
				<tr>
					<th>ID</th>
					<th>Email</th>
					<th>Password</th>
					<th>First name</th>
					<th>Last name</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${usersModel}" var="user">
					<tr>	
						<td><c:out value="${user.id}" /></td>				
						<td><c:out value="${user.email}" /></td>
						<td><c:out value="${user.password}" /></td>
						<td><c:out value="${user.firstname}" /></td>
						<td><c:out value="${user.lastname}" /></td>
						<td><a href="users/remove/${user.id}">remove</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</body>
</html>