## Lab 4 - Apache Tiles, Bootstrap

### Apache Tiles

#### Standard Layout

![LAYOUT](https://dl.dropboxusercontent.com/u/11650317/layout.png)

1. [Composite View pattern](https://tiles.apache.org/framework/tutorial/pattern.html) i [View Helper pattern](http://www.oracle.com/technetwork/java/viewhelper-139885.html)

2. [Apache Tiles](http://tiles.apache.org/)

------------

* Dodati Apache Tiles dependency-je u pom.xml

```xml
<!-- Tiles -->
<dependency>
	<groupId>org.apache.tiles</groupId>
	<artifactId>tiles-api</artifactId>
	<version>3.0.3</version>
</dependency>
<dependency>
	<groupId>org.apache.tiles</groupId>
	<artifactId>tiles-core</artifactId>
	<version>3.0.3</version>
</dependency>
<dependency>
	<groupId>org.apache.tiles</groupId>
	<artifactId>tiles-jsp</artifactId>
	<version>3.0.3</version>
</dependency>
```

* Unutar WEB-INF direktorijuma napraviti novi direktorijum tiles-def i u njemu napraviti XML fajl tiles.xml.
U tiles.xml ubaciti sledeći deo koda:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE tiles-definitions PUBLIC "-//Apache Software Foundation//DTD Tiles Configuration 3.0//EN" "http://tiles.apache.org/dtds/tiles-config_3_0.dtd">
<tiles-definitions>
	
</tiles-definitions>
```

* Izmeniti application-config.xml da koristi Tiles view resolver i dodati Tiles konfiguraciju:

```xml
<bean id="tilesViewResolver" class="org.springframework.web.servlet.view.tiles3.TilesViewResolver" />

<bean id="tilesConfigurer" class="org.springframework.web.servlet.view.tiles3.TilesConfigurer">
	<property name="definitions">
		<list>
			<value>/WEB-INF/tiles-def/tiles.xml</value>
		</list>
	</property>
</bean>
```

* U tiles.xml dodati definiciju standardnog layout-a:

```xml
<definition name="standardLayout" template="/WEB-INF/jsp/layout/standardLayout.jsp">
	<put-attribute name="title" value="" />
	<put-attribute name="content" value="" />
</definition>
```

* U WEB-INF/jsp direktorijumu napraviti tri nova direktorijuma: common, layout i content.
U common direktorijumu napraviti JSP stranicu tagLibs.jsp.
Ova JSP stranica treba da sadrži potrebne importe za razne JSP i JSTL tagove:

```xml
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

```

* U napravljenom WEB-INF/jsp/layout direktorijumu napraviti JSP stranicu standardLayout.jsp:

```html
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/tagLibs.jsp" %>

<tiles:importAttribute name="title" />
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title><fmt:message key="${title}" /></title>
	</head>
	<body>
		<tiles:insertAttribute name="content" />
	</body>
</html>
```


* Obratiti pažnju na elemente tiles:importAttribute i tiles:insertAttribute.

* Dodati definiciju za home view u tiles.xml:

```xml
<definition name="home" extends="standardLayout">
	<put-attribute name="title" value="page.home.title" />
	<put-attribute name="content" value="/WEB-INF/jsp/content/home.jsp" />
</definition>
```

* Izmeniti home.jsp da sadrži samo:

```html
<%@ include file="/WEB-INF/jsp/common/tagLibs.jsp" %>

<h1>WAFEPA - Home</h1>
<a href="activities">Activities</a>
<a href="users">Users</a>
```

* Refaktorisati postojeće view-ove za CRUD operacije nad aktivnostima da nasleđuju standardLayout.jsp i dodati potrebne definicije view-ova u tiles.xml

----

### Bootstrap

Bootstrap predstavlja biblioteku CSS (CSS3) klasa, kao i pomoćnih JavaScript funkcija.
Kompatibilan je isključivo sa HTML5 dokumentima. Baziran je na tzv. "responsive design" principu,
i omogućava lako prilagođavanje web aplikacije mobilnim uređajima (tablet, smartphone, ...).

Veoma je dobro dokumentovan, sa velikim brojem primera korišćenja. Bootstrap se može preuzeti na ovom [linku](http://getbootstrap.com/).

----

#### Statički resursi (CSS, jS) u Spring MVC

* U application-config.xml dodati sledeću liniju koda:

```xml
<mvc:resources location="/resources/" mapping="/resources/**" />
```

* U webapp direktorijumu napraviti direktorijum resources.

* Preuzeti sve fajlove iz assets direktorijuma na [GitLab sajtu projekta JWTS](https://gitlab.com/jocicmarko/jwts2-public/tree/master/assets).

* Iskopirati direktorijume css, js i fonts u prethodno napravljeni resources direktorijum.

* Izmeniti standardLayout.jsp:

```html
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/tagLibs.jsp" %>

<tiles:importAttribute name="title" />
<c:url value="/" var="root" />

<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title><fmt:message key="${title}" /></title>
		
		<!-- Bootstrap -->
    	<link href="${root}resources/css/bootstrap.css" rel="stylesheet">
    	<link href="${root}resources/css/bootstrap-theme.css" rel="stylesheet">
	</head>
	<body>
		<div class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="${root}">Home</a>
				</div>
			</div>
		</div>
		<div class="jumbotron">
			<div class="container text-center">
				<h1>WAFEPA</h1>
				<p>Web Application For Evidenting Physical Activities</p>
			</div>
		</div>

		<div class="container" style="padding: 4em">
			<tiles:insertAttribute name="content" />
		</div>
		
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	    <script src="${root}resources/js/jquery-2.1.1.js"></script>
	    <!-- Include all compiled plugins (below), or include individual files as needed -->
	    <script src="${root}resources/js/bootstrap.js"></script>
	</body>
</html>
```

* Obratiti pažnju na korišćene Boostrap CSS klase, kao i na obavezno navođenje
<!DOCTYPE HTML> pre html taga (ovo za HTML5 stranice mora biti navedeno).

* Izmeniti activities.jsp tako da html element table koristi Bootstrap CSS klase table i table-bordered.

* Izmeniti addEditActivity.jsp:

```html
<%@ include file="/WEB-INF/jsp/common/tagLibs.jsp" %>

<h1>WAFEPA - Add/edit activity</h1>

<c:url value="/activities" var="activitiesUrl" />
<form:form action="${activitiesUrl}" method="post" modelAttribute="activity" class="form-horizontal">

	<form:hidden path="id" />
	
	<div class="form-group">
		<form:label path="name" cssClass="col-sm-2">Name </form:label>
		<div class="col-sm-6">
			<form:input path="name" cssClass="form-control" />
		</div>
		<div class="col-sm-4">
			<span class="label label-danger"><form:errors path="name" /></span>
		</div>
	</div>		
	
	<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Save</button>
    </div>
  </div>
</form:form>
```

* Obratiti pažnju na korišćenje Bootstrap CSS klase.

----

### Domaći zadatak

1. Po uzoru na stranice za aktivnosti, izmeniti stranice za korisnike da koriste Apache Tiles.
Dodati potrebne definicije view-ova u tiles.xml.

2. Po uzoru na stranice za aktivnosti, izmeniti stranice za korisnika da koriste Bootstrap CSS klase.