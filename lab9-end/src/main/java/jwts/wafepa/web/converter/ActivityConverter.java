package jwts.wafepa.web.converter;

import java.util.ArrayList;
import java.util.List;

import jwts.wafepa.model.Activity;
import jwts.wafepa.web.dto.ActivityDTO;

import org.springframework.stereotype.Component;

@Component
public class ActivityConverter {

	public ActivityDTO activity2ActivityDTO(Activity activity) {
		ActivityDTO activityDTO = new ActivityDTO();
		activityDTO.setId(activity.getId());
		activityDTO.setName(activity.getName());
		return activityDTO;
	}
	
	public Activity activityDTO2Activity(ActivityDTO activityDTO) {
		Activity activity = new Activity();
		activity.setId(activityDTO.getId());
		activity.setName(activityDTO.getName());
		return activity;
	}
	
	public List<ActivityDTO> activities2ActivitiesDTO(List<Activity> activities) {
		List<ActivityDTO> activitiesDTO = new ArrayList<>();
		for (Activity activity : activities) {
			activitiesDTO.add(activity2ActivityDTO(activity));
		}
		return activitiesDTO;
	}
}
