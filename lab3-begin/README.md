## Lab 3 - Spring MVC CRUD, validacija formi

### Spring MVC CRUD

* Implementirati CRUD operacije nad aktivnostima i nad korisnicima
(dodati sve neophodne metode i anotacije u servisnom sloju kao i u kontrolerima)

#### Stranica za dodavanje/izmenu aktivnosti (addEditActivity.jsp)

Dodati tagove:

```html
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
```

----


### Validacija formi

U Spring-u, [validacija podataka](http://docs.spring.io/spring/docs/4.0.2.RELEASE/spring-framework-reference/htmlsingle/#validation-beanvalidation) se vr�i anotiranjem klasa i odredenih polja klasa.
Spring Framework nudi velik skup predefinsanih validatora sa njima odgovarajucim porukama gre�ke.
Validacija formi je direktno podr�ana kroz biblioteku form tagova.

----

* Dodati potrebne dependency-je za validaciju

```xml
<!-- Validation -->
<dependency>
	<groupId>javax.validation</groupId>
	<artifactId>validation-api</artifactId>
	<version>1.1.0.Final</version>
</dependency>
<dependency>
	<groupId>org.hibernate</groupId>
	<artifactId>hibernate-validator</artifactId>
	<version>5.1.0.Final</version>
</dependency>
```

* Anotirati polje name u klasi Activity sa @NotEmpty i @Length(max=30)

* Izmeniti post metodu ActivityController-a tako da je Activity parametar anotiran sa @Valid.
Takode, kao parametre dodati BindingResult i Model. Parametar BindingResult omogucava proveru da li su podaci anotirani sa @Valid validni.
Parametar Model omogucava prosledivanje poruke o gre�ci nazad na view.

```java
@RequestMapping(method = RequestMethod.POST)
public String post(@Valid Activity activity, BindingResult bindingResult, Model model) {
...
}
```

* Izmeniti view za dodavanje/izmenu aktivnosti tako da koristi tagove za poruke gre�ke (form:errors)

----

#### "Custom" poruke gre�ke

* Unutar src/main/resources napraviti direktorijum cfg, pa unutar njega direktorijum messsages.

* Unutar cfg/messages direktorijuma napraviti fajl messages.properties

* Dodati konfiguraciju u application-context.xml

```xml
<!-- Messages -->
<bean id="messageSource" class="org.springframework.context.support.ResourceBundleMessageSource"
	p:basename="cfg.messages.messages" />
```