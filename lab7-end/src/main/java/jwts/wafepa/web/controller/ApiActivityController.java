package jwts.wafepa.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import jwts.wafepa.model.Activity;
import jwts.wafepa.service.ActivityService;
import jwts.wafepa.web.converter.ActivityConverter;
import jwts.wafepa.web.dto.ActivityDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/api/activities")
public class ApiActivityController {

	@Autowired
	private ActivityService activityService;
	@Autowired
	private ActivityConverter activityConverter;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<ActivityDTO>> getActivities() {
		List<Activity> activities = activityService.findAll();
		List<ActivityDTO> activitiesDTO = new ArrayList<>();
		for (Activity activity : activities) {
			activitiesDTO.add(activityConverter.activity2ActivityDTO(activity));
		}
		return new ResponseEntity<>(activitiesDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<ActivityDTO> getActivity(@PathVariable Long id) {
		Activity activity = activityService.findOne(id);
		if (activity != null) {
			ActivityDTO activityDTO = activityConverter.activity2ActivityDTO(activity);
			return new ResponseEntity<>(activityDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<ActivityDTO> deleteActivity(@PathVariable Long id) {
		Activity activity = activityService.findOne(id);
		if (activity != null) {
			ActivityDTO activityDTO = activityConverter.activity2ActivityDTO(activity);
			activityService.remove(id);
			return new ResponseEntity<>(activityDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(consumes="application/json", method=RequestMethod.POST)
	public ResponseEntity<ActivityDTO> saveActivity(@RequestBody @Valid ActivityDTO activityDTO) {
		Activity activity = activityService.save(activityConverter.activityDTO2Activity(activityDTO));
		activityDTO = activityConverter.activity2ActivityDTO(activity);
		return new ResponseEntity<>(activityDTO, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/{id}", consumes="application/json", method=RequestMethod.PUT)
	public ResponseEntity<ActivityDTO> editActivity(@PathVariable Long id, @RequestBody @Valid ActivityDTO activityDTO) {
		if (activityService.findOne(id) != null) {
			if (id != activityDTO.getId()) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			} else {
				Activity activity = activityService.save(activityConverter.activityDTO2Activity(activityDTO));
				activityDTO = activityConverter.activity2ActivityDTO(activity);
				return new ResponseEntity<>(activityDTO, HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
