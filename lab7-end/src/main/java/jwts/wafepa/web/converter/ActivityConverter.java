package jwts.wafepa.web.converter;

import org.springframework.stereotype.Component;

import jwts.wafepa.model.Activity;
import jwts.wafepa.web.dto.ActivityDTO;

@Component
public class ActivityConverter {

	public ActivityDTO activity2ActivityDTO(Activity activity) {
		ActivityDTO activityDTO = new ActivityDTO();
		activityDTO.setId(activity.getId());
		activityDTO.setName(activity.getName());
		return activityDTO;
	}
	
	public Activity activityDTO2Activity(ActivityDTO activityDTO) {
		Activity activity = new Activity();
		activity.setId(activityDTO.getId());
		activity.setName(activityDTO.getName());
		return activity;
	}
}
