﻿## Lab 7 - DTO, SPA

----


Classical web - server side generated HTML:
![Server side generated HTML](https://gitlab.levi9.com/d.gajic/code9/raw/master/img/web-centric.png)


Data centric approach - server side generated data:
![Data Centric](https://gitlab.levi9.com/d.gajic/code9/raw/master/img/data-centric.png)


### REST

----

REST web servis (često nazivan i REST web API) je web servis implementiran korišćenjem HTTP protokola i REST (Representation State Transfer) principa - [What is REST](http://martinfowler.com/articles/richardsonMaturityModel.html).
REST web servis predstavlja kolekciju resursa, sa sledećim aspektima:

1. bazna URL adresa web servisa
2. tipovi podataka koji se primaju/šalju od strane web servisa su JSON i/ili XML
3. skup operacija podržanih od strane web servisa korišćenjem HTTP (npr. GET, PUT, POST, DELETE)

HTTP protokol ima bogat vokabular metoda:

- GET
- POST
- PUT
- DELETE

Korišćenjem ovog vokabulara API (web servis) se dizajnira tako da su je svaki entitet (resurs)
potreban samo njegov URL za vršenje CRUD operacija preko različitih HTTP metoda:
- ako se traži kolekcija resursa: /activities
- ako se traži konkretan resurs: /activities/{id}, gde id predstavlja identifikator resursa

----

### DTO (Data Transfer Object)

Data Transfer Object je objekat koji se koristi za enkapsulaciju podataka, 
i za njihovo slanje/prijem od strane jednog podsistema aplikacije ka drugom podsistemu.

DTO se najčešće koriste u višeslojnoj aplikaciji za transfer podataka između same aplikacije i UI sloja.
Takođe se veoma dobro uklapaju u MVC šablon.

----

* Napraviti paket jwts.wafepa.web.dto

* Napraviti klasu ActivityDTO u paketu jwts.wafepa.web.dto. Polja klase ActivityDTO treba da budu ista kao polja klase Activity (Long id, String name).

* Prebaciti sve anotacije koje se koriste za validaciju podataka iz Activity u ActivityDTO.

* Izmeniti ApiActivityController da ne koristi Activity za prenos podataka između UI sloja i aplikacije, već ActivityDTO.

* Testirati napravljeni REST web servis u web browseru.

----

### SPA (Single Page Application)

U SPA, ceo potreban kod - HTML, JavaScript i CSS se učitaju odjednom, na jednoj stranici (otuda naziv Single Page Application).
Reload stranica se ne radi ni u kom trenutku, kao ni prebacivanje na neku drugu stranicu ununtar aplikacije - stvara se samo privid
reload-ovanja stranica i navigacije kao ostalim stranicama.
Interakcija sa SPA uključuje stalnu komunikaciju aplikacije sa (REST) web servisom u pozadini.

1. (SPA and the Single Page Myth)[http://www.johnpapa.net/pageinspa/]
2. (SPA-wiki)[http://en.wikipedia.org/wiki/Single-page_application]

Glavna ideja SPA je da se renderuju i menjaju samo oni elementi korisničkog interfejsa za koje je to potrebno.
Menjanje elemenata korisničkog interfejsa uglavnom podrazumeva intenzivnu manipulaciju HTML-om i DOM-om unutar stranice.

Postoji veliki broj JavaScript biblioteka koje služe za pravljenje SPA (Angular.js, Backbone.js, Ember.js...). U ovom projektu
će biti korišćen Backbone.js.

----

### Domaći zadatak

1. Po uzoru na aktivitnosti, napraviti REST web servis za korisnike
2. Napraviti UserDTO klasu i izmeniti REST web servis za korisnike da koristi UserDTO, a ne User klasu.
3. Testirati napravljeni REST web servis u web browseru.