package jwts.wafepa.service;

import java.util.List;

import jwts.wafepa.model.Activity;
import jwts.wafepa.service.impl.InMemoryActivityService;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class InMemoryActivityServiceTest {

	private ActivityService activityService;
	
	@Before
	public void setUp() {
		activityService = new InMemoryActivityService();
		
		Activity activityRunning = new Activity();
		activityRunning.setId(1L);
		activityRunning.setName("Running");
		
		Activity activitySwimming = new Activity();
		activitySwimming.setId(2L);
		activitySwimming.setName("Swimming");
		
		activityService.save(activityRunning);
		activityService.save(activitySwimming);
	}
	
	@Test
	public void testFindOne() {
		Activity activity = activityService.findOne(1L);
		Assert.assertNotNull(activity);
		Assert.assertEquals("Running", activity.getName());
	}
	
	@Test
	public void testFindAll() {
		List<Activity> cats = activityService.findAll();
		Assert.assertEquals(2, cats.size());
		Activity cat1 = cats.get(0);
		Activity cat2 = cats.get(1);
		if (cat1.getId().equals(1L)) {
			Assert.assertEquals("Running", cat1.getName());
			Assert.assertTrue(cat2.getId().equals(2L) && cat2.getName().equals("Swimming"));
		} else {
			Assert.assertTrue(cat1.getId().equals(2L) && cat1.getName().equals("Swimming"));
			Assert.assertTrue(cat2.getId().equals(1L) && cat2.getName().equals("Running"));
		}
	}

	@Test
	public void testSave() {
		Activity cat = new Activity();
		cat.setName("New activity");
		Activity saved = activityService.save(cat);
		
		Assert.assertNotNull(saved.getId());		
		Assert.assertEquals("New activity", activityService.findOne(saved.getId()).getName());
	}

	@Test
	public void testRemove() {
		Assert.assertNotNull(activityService.findOne(1L));
		Assert.assertNotNull(activityService.findOne(2L));
		
		activityService.remove(1L);
		
		Assert.assertNull(activityService.findOne(1L));
		Assert.assertNotNull(activityService.findOne(2L));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testRemoveIllegalArgument() {
		Assert.assertNull(activityService.findOne(3L));		
		activityService.remove(3L);
	}
	
	@After
	public void tearDown() {
		// free resources
	}
}
