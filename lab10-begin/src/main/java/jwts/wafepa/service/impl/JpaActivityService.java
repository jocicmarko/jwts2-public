package jwts.wafepa.service.impl;

import java.util.Date;
import java.util.List;

import jwts.wafepa.model.Activity;
import jwts.wafepa.repository.ActivityRepository;
import jwts.wafepa.service.ActivityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JpaActivityService implements ActivityService {

	@Autowired
	private ActivityRepository activityRepository;
	
	@Override
	public Activity findOne(Long id) {
		return activityRepository.findOne(id);
	}

	@Override
	public List<Activity> findAll() {
		return activityRepository.findAll();
	}

	@Override
	public Activity save(Activity activity) {
		if (activity.getId() == null) { // creating new object
			activity.setCreated(new Date());
		}
		return activityRepository.save(activity);
	}

	@Override
	public void remove(Long id) throws IllegalArgumentException {
		activityRepository.delete(id);
	}

}
