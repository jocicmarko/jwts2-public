package jwts.wafepa.web.controller;

import java.util.List;

import javax.validation.Valid;

import jwts.wafepa.model.User;
import jwts.wafepa.service.UserService;
import jwts.wafepa.web.converter.UserConverter;
import jwts.wafepa.web.dto.UserDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/api/users")
public class ApiUserController {

	@Autowired
	private UserService userService;
	@Autowired
	private UserConverter userConverter;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<UserDTO>> getUsers() {
		List<User> users = userService.findAll();
		List<UserDTO> usersDTO = userConverter.users2UsersDTO(users);
		return new ResponseEntity<>(usersDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<UserDTO> getUser(@PathVariable Long id) {
		User user = userService.findOne(id);
		if (user != null) {
			UserDTO userDTO = userConverter.user2UserDTO(user);
			return new ResponseEntity<>(userDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<UserDTO> deleteUser(@PathVariable Long id) {
		User user = userService.findOne(id);
		if (user != null) {
			UserDTO userDTO = userConverter.user2UserDTO(user);
			userService.remove(id);
			return new ResponseEntity<>(userDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(consumes="application/json", method=RequestMethod.POST)
	public ResponseEntity<UserDTO> saveUser(@RequestBody @Valid UserDTO userDTO) {
		User user = userService.save(userConverter.userDTO2User(userDTO));
		userDTO = userConverter.user2UserDTO(user);
		return new ResponseEntity<>(userDTO, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/{id}", consumes="application/json", method=RequestMethod.PUT)
	public ResponseEntity<UserDTO> editUser(@PathVariable Long id, @RequestBody @Valid UserDTO userDTO) {
		if (userService.findOne(id) != null) {
			if (id != userDTO.getId()) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			} else {
				User user = userService.save(userConverter.userDTO2User(userDTO));
				userDTO = userConverter.user2UserDTO(user);
				return new ResponseEntity<>(userDTO, HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
