$(document).ready(function() {
	
	/* Home page view */
	HomePageView = Backbone.View.extend({
		el : $('#container'), // "el" označava za koji deo HTML stranice je zadužen ovaj view, odnosno gde će se prikazivati
		template : _.template($('#homePageView').html()), // template je u templates.jsp unutar <script type="text/template" id="homePageView"> taga
		initialize : function() { // "initialize" je konstruktor za view
			this.render();
		},
		render : function() { // "render" je funkcija za iscrtavanja view-a
			$(this.el).html(this.template()); // ubacivanje HTML-a definisanog u template unutar el elementa
		}
	});
	
	/* Activity */
	ActivityModel = Backbone.Model.extend({
		urlRoot : apiRoot + 'activities', // "urlRoot" je korenska adresa na REST API (web servisu) za ovaj entitet
		defaults : { // podrazumevane vrednosti polja, Backbone model mora da se poklapa sa DTO modelom
			id : null,
			name : ''
		}
	});
	
	ActivityCollection = Backbone.Collection.extend({
		model : ActivityModel, // ovo je kolekcija Activity modela
		url : apiRoot + 'activities' // adresa na REST API gde se nalazi ova kolekcija
	});
	
	/* Activities page view */
	ActivitiesPageView = Backbone.View.extend({
		el : $('#container'), // "el" označava za koji deo HTML stranice je zadužen ovaj view, odnosno gde će se prikazivati
		template : _.template($('#activitiesPageView').html()), // template je u templates.jsp unutar <script type="text/template" id="activitiesPageView"> taga
		initialize : function() {
			this.render(); // iscrtaj stranicu
			this.getActivities(); // zatim preuzmi aktivnosti kako bi se iscrtale
		},
		render : function() {
			$(this.el).html(this.template()); // ubacivanje HTML-a definisanog u template unutar el elementa
		},
		getActivities : function() { // metoda koja dobavlja aktivnisti sa servera
			new ActivityCollectionView({ el : $('#activitiesTable tbody')}); // pravljenje ActivityCollectionView-a, i setovanje el elementa kroz parametre konstruktora
		}
	});
	
	/* Activity collection view */
	ActivityCollectionView = Backbone.View.extend({
		model : new ActivityCollection, // model koji će prikazivati ovaj view je ActivityCollection
		initialize : function() {
			var self = this; // kako bi imali referencu na this unutar callback funkcije
			this.model.fetch({ // poziva se GET /api/activities
				success : function() { // callback ako je bilo 200 OK
					self.render(); // aktivnosti su dobavljene, preko self imamo referencu na this, pa možemo da pozovemo render
				},
				error : function() { // callback ako nije bilo 200 OK

				}
			});
		},
		render : function() {
			for (var key in this.model.models) { // uzimanje aktivnosti jednu po jednu
				var activity = this.model.models[key]; // pošto je this.model.models hash, konkretnu aktivnost uzimamo po ključu
				var activityItemView = new ActivityItemView({ model : activity }); // za tu aktivnost pravimo ActivityItemView (jedan red u tabeli), dinamički setujemo model kroz parametar konstruktora
				var domEl = activityItemView.render().el; // poziva se render za ActivityItemView, što izgeneriše HTML za taj red u tabeli, i .el vraća taj red kao DOM element
				$(this.el).append(domEl); // dodajemo red tabele unutar tbody
			}
		}
	});
	
	/* Activity item view */
	ActivityItemView = Backbone.View.extend({
		tagName : "tr", // el nije eksplicitno specificiran, već je samo rečeno da će ovaj view biti unutar nekog tr taga
		template : _.template($('#activityItemView').html()), // template je u templates.jsp unutar <script type="text/template" id="activityItemView"> taga
		events : { // definisanje event-ova (događaja) u formatu "vrsta_eventa element" : "metoda"
			"click .removeActivity" : "removeActivity"
		},
		initialize : function() {
			
		},
		render : function() {
			$(this.el).html(this.template({ activity : this.model.toJSON() })); // generisanje HTML preko template-a, ali uz dodatni podatak - activity, koji sadrži id i name aktivnosti
																				// this.model je zapravo aktivnost koja je postavljenja kroz parametar konstruktora (linija 62)
			return this; // vraćamo this kako bismo mogli da vratimo el ovog view-a (linija 63)
		},
		removeActivity : function() {
			this.model.destroy(); // poziva DELETE /api/activities/{id}
			$(this.el).remove(); // brisanje ovog view-a, odnosno el koji ga predstavlja iz DOM stabla
			return false; // return false zaustavlja propagaciju događaja klika, odnosno pošto se removeActivity izvršava na klik na <a href="#activities/remove/{id}>
						  // po defaultu će browser nakon klika da ubaci tu adresu u adress bar browsera, a zapravo želimo da ostanemo na stranici #activities
		}
	});
	
	/* Router */
	AppRouter = Backbone.Router.extend({ // Backbone router je zapravo kontroler unutar klijentske MVC aplikacije (tj. ove Backbone aplikacije)
		routes : { // definisanje ruta u formatu "ruta" : "metoda". "ruta" je u browseru zapravo #ruta
			"" : "homeRoute",
			"activities" : "activitiesRoute"
		},
		homeRoute : function() {
			new HomePageView;
		},
		activitiesRoute : function() {
			new ActivitiesPageView;
		}
	});
	
	var appRouter = new AppRouter; // obavezno da bi aplikacija radila
	
	Backbone.history.start(); // omogućava privid navigacije kroz stranice preko hashtagova (#), iako samo zapravo stalno na jednoj stranici
});