<script type="text/template" id="homePageView">
<div class="container text-center">
	<!-- Example row of columns -->
	<h1>WAFEPA - Home</h1>
	<div class="row">
		<div class="col-md-6">
			<h2>Activities</h2>
			<p>CRUD operations on activities</p>
			<p><a class="btn btn-default" href="#activities">Go to activities</a>
			</p>
		</div>
		<div class="col-md-6">
			<h2>Users</h2>
			<p>CRUD operations on users</p>
			<p><a class="btn btn-default" href="#users">Go to users</a>
			</p>
		</div>
	</div>
	<hr>
</div>
</script>

<script type="text/template" id="activitiesPageView">
<h1>Activities</h1>
<table id="activitiesTable" class="table table-bordered">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
</script>

<script type="text/template" id="activityItemView">
<td><@= activity.id @></td>				
<td><@= activity.name @></td>
<td>
	<a href="#activities/remove/<@= activity.id @>" class="removeActivity">remove</a>
	<a href="#activities/edit/<@= activity.id @>">edit</a>
</td>
</script>