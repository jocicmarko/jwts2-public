﻿## Lab 6 - REST web service, DTO

----


Classical web - server side generated HTML:
![Server side generated HTML](https://gitlab.levi9.com/d.gajic/code9/raw/master/img/web-centric.png)


Data centric approach - server side generated data:
![Data Centric](https://gitlab.levi9.com/d.gajic/code9/raw/master/img/data-centric.png)


### REST

----

REST web servis (često nazivan i REST web API) je web servis implementiran korišćenjem HTTP protokola i REST (Representation State Transfer) principa - [What is REST](http://martinfowler.com/articles/richardsonMaturityModel.html).
REST web servis predstavlja kolekciju resursa, sa sledećim aspektima:

1. bazna URL adresa web servisa
2. tipovi podataka koji se primaju/šalju od strane web servisa su JSON i/ili XML
3. skup operacija podržanih od strane web servisa korišćenjem HTTP (npr. GET, PUT, POST, DELETE)

HTTP protokol ima bogat vokabular metoda:

- GET
- POST
- PUT
- DELETE

Korišćenjem ovog vokabulara API (web servis) se dizajnira tako da su je svaki entitet (resurs)
potreban samo njegov URL za vršenje CRUD operacija preko različitih HTTP metoda:
- ako se traži kolekcija resursa: /activities
- ako se traži konkretan resurs: /activities/{id}, gde id predstavlja identifikator resursa

----

#### Spring MVC i REST

1. [How to expose data via REST/JSON using Spring MVC](https://spring.io/guides/tutorials/rest/)
2. [Spring REST support](http://docs.spring.io/spring/docs/4.0.3.RELEASE/spring-framework-reference/htmlsingle/#mvc-ann-restcontroller)

----

* Napraviti ApiActivityController u paketu jwts.wafepa.web.controller. Staviti da se ovaj kontroler mapira na URL "api/activities" i anotirati ga sa @RestController.

* Implementirati CRUD operacije za aktivnosti prateći REST princip.

* Kako bi se podaci (objekti) koje vraća/prihvata REST web servis serijalizovali u JSON objekte,
potrebno je dodati dependency za Jackson mapper (koji mapira Java POJO objekte na JSON, i obrnuto):

```xml
<!-- JSON -->
<dependency>
	<groupId>org.codehaus.jackson</groupId>
	<artifactId>jackson-mapper-asl</artifactId>
	<version>1.9.13</version>
</dependency>
<dependency>
	<groupId>org.codehaus.jackson</groupId>
	<artifactId>jackson-core-asl</artifactId>
	<version>1.9.13</version>
</dependency>
<dependency>
	<groupId>org.codehaus.jackson</groupId>
	<artifactId>jackson-jaxrs</artifactId>
	<version>1.9.13</version>
</dependency>
```

* Testirati napravljeni REST web servis u web browseru.

----

### DTO (Data Transfer Object)

Data Transfer Object je objekat koji se koristi za enkapsulaciju podataka, 
i za njihovo slanje/prijem od strane jednog podsistema aplikacije ka drugom podsistemu.

DTO se najčešće koriste u višeslojnoj aplikaciji za transfer podataka između same aplikacije i UI sloja.
Takođe se veoma dobro uklapaju u MVC šablon.

----

* Napraviti paket jwts.wafepa.web.dto

* Napraviti klasu ActivityDTO u paketu jwts.wafepa.web.dto. Polja klase ActivityDTO treba da budu ista kao polja klase Activity (Long id, String name).

* Prebaciti sve anotacije koje se koriste za validaciju podataka iz Activity u ActivityDTO.

* Izmeniti ApiActivityController da ne koristi Activity za prenos podataka između UI sloja i aplikacije, već ActivityDTO.

* Testirati napravljeni REST web servis u web browseru.

----

### Domaći zadatak

1. Po uzoru na aktivitnosti, napraviti REST web servis za korisnike
2. Napraviti UserDTO klasu i izmeniti REST web servis za korisnike da koristi UserDTO, a ne User klasu.
3. Testirati napravljeni REST web servis u web browseru.