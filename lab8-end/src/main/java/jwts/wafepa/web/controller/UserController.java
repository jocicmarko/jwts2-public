package jwts.wafepa.web.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

import jwts.wafepa.model.User;
import jwts.wafepa.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/users")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@PostConstruct
	private void init() {

	}

	@RequestMapping(method=RequestMethod.GET)
	public String getUser(Model model) {
		List<User> users = userService.findAll();
		model.addAttribute("usersModel", users);
		return "users";
	}
	
	@RequestMapping(value="/remove/{id}")
	public String remove(@PathVariable Long id) {
		userService.remove(id);
		return "redirect:/users";
	}
	
	@RequestMapping(value="/edit/{id}")
	public String edit(@PathVariable Long id, Model model) {
		model.addAttribute("user", userService.findOne(id));
		return "addEditUser";
	}
	
	
	@RequestMapping(value="/add")
	public String add(Model model) {
		model.addAttribute("user", new User());
		return "addEditUser";
	}
	@RequestMapping(method=RequestMethod.POST)
	public String post(@Valid User user, BindingResult bindingResult, Model model) {
		
		if (bindingResult.hasErrors()) {
			model.addAttribute("user", user);
			return "addEditUser";
		} else {
			userService.save(user);
			return "redirect:/users";
		}
	}
}
