$(document).ready(function() {
	
	/* Home page view */
	HomePageView = Backbone.View.extend({
		
		el : $('#container'),
		template : _.template($('#homePageView').html()),
		initialize : function() {
			this.render();
		},
		render : function() {
			$(this.el).html(this.template());
		}
	});
	
	/* Activity */
	ActivityModel = Backbone.Model.extend({
		urlRoot : apiRoot + 'activities',
		defaults : {
			id : null,
			name : ''
		}
	});
	
	ActivityCollection = Backbone.Collection.extend({
		model : ActivityModel,
		url : apiRoot + 'activities'
	});
	
	/* Router */
	AppRouter = Backbone.Router.extend({
		routes : {
			"" : "homeRoute"
		},
		homeRoute : function() {
			new HomePageView;
		}
	});
	
	var appRouter = new AppRouter;
	
	Backbone.history.start();
});