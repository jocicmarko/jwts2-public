<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1>Activities</h1>

<table class="table table-bordered">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${activitiesModel}" var="activity">
			<tr>	
				<td><c:out value="${activity.id}" /></td>				
				<td><c:out value="${activity.name}" /></td>
				<td>
					<a href="activities/remove/${activity.id}">remove</a>
					<a href="activities/edit/${activity.id}">edit</a>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<a href="activities/add">Add activity</a>