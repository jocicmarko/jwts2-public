package jwts.wafepa.web.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import jwts.wafepa.model.User;
import jwts.wafepa.web.dto.UserDTO;

@Component
public class UserConverter {

	public UserDTO user2UserDTO(User user) {
		UserDTO userDTO = new UserDTO();
		userDTO.setId(user.getId());
		userDTO.setEmail(user.getEmail());
		userDTO.setPassword(user.getPassword());
		userDTO.setFirstname(user.getFirstname());
		userDTO.setLastname(user.getLastname());
		return userDTO;
	}
	
	public User userDTO2User(UserDTO userDTO) {
		User user = new User();
		user.setId(user.getId());
		user.setEmail(userDTO.getEmail());
		user.setPassword(userDTO.getPassword());
		user.setFirstname(userDTO.getFirstname());
		user.setLastname(user.getLastname());
		return user;
	}
	
	public List<UserDTO> users2UsersDTO(List<User> users) {
		List<UserDTO> usersDTO = new ArrayList<>();
		for (User user : users) {
			usersDTO.add(user2UserDTO(user));
		}
		return usersDTO;
	}
}
